//---------------------------
// Programming Assignment #1
// Victor Zordan
//
// Cooper Wolfe (cswolfe)
// Due 10/4/17
//---------------------------

// Include needed files
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h> // The GL Utility Toolkit (Glut) Header

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

// Constants
#define WIDTH 500
#define HEIGHT 500

// Types
typedef double * Vector;
typedef Vector * Matrix;
struct Pixel {
	double r, g, b;
	Pixel() { r = g = b = 0.0; }
	Pixel(double _r, double _g, double _b) { r = _r; g = _g; b = _b; }
};
struct Vertex {
	Vector vec;
	Vertex * next;
	int length;
	Vertex(Vector v, Vertex * n) { vec = v; next = n; length = n ? n->length+1 : 1; }
};
struct Face {
	Vertex * pts;
	Face * next;
	int length;
	Pixel * color;
	Face(Vertex * p, Face * n) {
		pts = p;
		next = n;
		length = n ? n->length+1 : 1;
		color = new Pixel(
			rand() % 0xff / 255.0,
			rand() % 0xff / 255.0,
			rand() % 0xff / 255.0
		);
	}
};
struct ActiveEdgeList {
	int y_upper;
	double x, x_inc;
	ActiveEdgeList * next;
	ActiveEdgeList(int yu, double _x, double inc, ActiveEdgeList * n) { y_upper = yu; x = _x; x_inc = inc; next = n; }
};
struct EdgeTable {
	int y;
	ActiveEdgeList * ael;
	EdgeTable * next;
	EdgeTable(int _y, ActiveEdgeList * a, EdgeTable * n) { y = _y; ael = a; next = n; }
};
enum Command {
	NONE,
	WIREFRAME,
	TRANSLATE,
	ROTATE,
	SCALE,
	UP,
	DOWN,
	LEFT,
	RIGHT
};

// Global data structures
Matrix frame_buffer;
Vertex * vertices = NULL;
Face * faces = NULL;
double furthest = 0.0;
bool wire = false;
Vector center = new double[4];
Command mode = NONE;


/******************
   --- Helpers ---
 ******************/
// OpenGL
// Color in pixel at (x,y) with specified color
void write_pixel(int x, int y, Pixel * color) {
	glColor3f (color->r, color->g, color->b);
	glBegin(GL_POINTS);
	glVertex3i(x, y, 0);
	glEnd();
}

// MATRIX MATH
// Apply matrix m to each vertex in vertices
void apply_matrix(Matrix m) {
	for (Vertex * curr = vertices; curr; curr = curr->next) {
		Vector result = new double[4];
		for (int i = 0; i < 4; ++i) {
			result[i] = 0.0;
			for (int j = 0; j < 4; ++j)
				result[i] += curr->vec[j] * m[j][i];
		}
		for (int i = 0; i < 4; ++i)
			curr->vec[i] = result[i];
	}
}

// Deletes matrix
void delete_matrix(Matrix m) {
	for (int i = 0; i < 4; ++i)
		delete[] m[i];
	delete m;
}

// VECTOR MATH
// Returns difference between v1 and v2
double distance(Vector v1, Vector v2) {
	double x = v2[0] - v1[0], y = v2[1] - v1[1], z = v2[2] - v1[2];
	return sqrt(x*x + y*y + z*z);
}

// Returns slope of line between vertices v1 and v2
double slope(Vector v1, Vector v2) {
	Vector min = v1[0] < v2[0] ? v1 : v2;
	Vector max = min == v1 ? v2 : v1;
	return (max[1] - min[1]) / (max[0] - min[0]);
}

// Returns difference between v1 and v2
Vector vec_sub(Vector v1, Vector v2) {
	Vector v = new double[4];
	for (int i = 0; i < 4; ++i)
		v[i] = v1[i] - v2[i];
	return v;
}

// Returns sum of v1 and v2
Vector vec_add(Vector v1, Vector v2) {
	Vector v = new double[4];
	for (int i = 0; i < 4; ++i)
		v[i] = v1[i] + v2[i];
	return v;
}

// Returns cross product of v1 and v2
Vector vec_cross(Vector v1, Vector v2) {
	Vector cross = new double[4];
	cross[0] = v1[1] * v2[2] - v1[2] * v2[1];
	cross[1] = v1[2] * v2[0] - v1[0] * v2[2];
	cross[2] = v1[0] * v2[1] - v1[1] * v2[0];
	cross[3] = 0.0;
	return cross;
}

// Returns scalar product of s and v1
Vector vec_mul(double s, Vector v1) {
	Vector v = new double[4];
	for (int i = 0; i < 4; ++i)
		v[i] = s * v1[i];
	return v;
}

// Updates the global center vector to the average of the points in vertices
void get_center() {
	for (int i = 0; i < 4; ++i) center[i] = 0.0;
	for (Vertex * curr = vertices; curr; curr = curr->next)
		for (int i = 0; i < 4; ++i)
			center[i] += curr->vec[i];
	for (int i = 0; i < 4; ++i) center[i] /= vertices->length;
}

// PLANE MATH
// Returns normal vector of triangular face
Vector get_normal(Face * face) {
	Vector v1 = vec_sub(face->pts->vec, face->pts->next->vec);
	Vector v2 = vec_sub(face->pts->vec, face->pts->next->next->vec);
	Vector n = vec_cross(v1, v2);
	delete[] v1; delete[] v2;
	return n;
}


/*******************
   --- Matrices ---
 *******************/
// TRANSLATION
// Applies a translation matrix in specified direction
void translate(double dx, double dy, double dz) {
	// Create matrix
	Matrix t = new Vector[4];
	for (int i = 0; i < 4; ++i) {
		t[i] = new double[4];
		for (int j = 0; j < 4; ++j)
			if (i == j) t[i][j] = 1.0;
			else if (i == 3) {
				switch (j) {
					case 0: t[i][j] = dx; break;
					case 1: t[i][j] = dy; break;
					case 2: t[i][j] = dz; break;
				}
			}
			else t[i][j] = 0.0;
	}

	// Apply and delete matrix
	apply_matrix(t);
	delete_matrix(t);
}

// Updates center and translates shape to origin
void translate_to_origin() {
	get_center();
	translate(-center[0], -center[1], -center[2]);
}

// Translates shape to center
void translate_back() { translate(center[0], center[1], center[2]); }

// Translates shape to the middle of the window
void translate_to_center() {
	translate_to_origin();
	translate(WIDTH / 2, HEIGHT / 2, furthest);
}

// SCALING
// Applies a scale matrix with specified dimension scale factors
void scale(double sx, double sy, double sz) {
	// Create matrix
	Matrix s = new Vector[4];
	for (int i = 0; i < 4; ++i) {
		s[i] = new double[4];
		for (int j = 0; j < 4; ++j)
			if (i == j)
				switch (i) {
					case 0: s[i][j] = sx; break;
					case 1: s[i][j] = sy; break;
					case 2: s[i][j] = sz; break;
					case 3: s[i][j] = 1.0;
				}
			else s[i][j] = 0.0;
	}

	// Apply and delete matrix
	apply_matrix(s);
	delete_matrix(s);
}

// Applies a scale matrix with specified scale factor (to all dimensions)
void scale(double s) { scale(s, s, s); }

// ROTATION
// Applies a rotation matrix about the x-axis of specified amount
void rotate_x(double theta) {
	// Create matrix
	Matrix r = new Vector[4];
	for (int i = 0; i < 4; ++i) {
		r[i] = new double[4];
		for (int j = 0; j < 4; ++j)
			if (!(i|j) || (i == 3 && j == 3)) r[i][j] = 1.0;
			else if (i == j) r[i][j] = cos(theta);
			else if (i == 2 && j == 1) r[i][j] = -sin(theta);
			else if (i == 1 && j == 2) r[i][j] = sin(theta);
			else r[i][j] = 0.0;
	}

	// Apply and delete matrix
	apply_matrix(r);
	delete_matrix(r);
}

// Applies a rotation matrix about the y-axis of specified amount
void rotate_y(double theta) {
	// Create matrix
	Matrix r = new Vector[4];
	for (int i = 0; i < 4; ++i) {
		r[i] = new double[4];
		for (int j = 0; j < 4; ++j)
			if ((i == 1 && j == 1) || (i == 3 && j == 3)) r[i][j] = 1.0;
			else if (i == j) r[i][j] = cos(theta);
			else if (!i && j == 2) r[i][j] = -sin(theta);
			else if (i == 2 && !j) r[i][j] = sin(theta);
			else r[i][j] = 0.0;
	}

	// Apply and delete matrix
	apply_matrix(r);
	delete_matrix(r);
}

// Applies a rotation matrix about the z-axis of specified amount
void rotate_z(double theta) {
	// Create matrix
	Matrix r = new Vector[4];
	for (int i = 0; i < 4; ++i) {
		r[i] = new double[4];
		for (int j = 0; j < 4; ++j)
			if ((i == 2 && j == 2) || (i == 3 && j == 3)) r[i][j] = 1.0;
			else if (i == j) r[i][j] = cos(theta);
			else if (i == 1 && !j) r[i][j] = -sin(theta);
			else if (!i && j == 1) r[i][j] = sin(theta);
			else r[i][j] = 0.0;
	}

	// Apply and delete matrix
	apply_matrix(r);
	delete_matrix(r);
}


/*******************
  --- Rendering ---
 *******************/
// Gets edge table for triangular face
EdgeTable * get_edge_table(Face * face) {
	// Sort vertices
	Vertex * min, * max, * median; min = max = median = face->pts;
	for (Vertex * v = face->pts->next; v; v = v->next) {
		if (v->vec[1] < min->vec[1]) min = v;
		else if (v->vec[1] == min->vec[1] && v->vec[0] <  min->vec[0]) min = v;
		if (v->vec[1] > max->vec[1]) max = v;
		else if (v->vec[1] == max->vec[1] && v->vec[0] > max->vec[0]) max = v;
	}
	for (int i = 0; i < 2; ++i) median = median == max || median == min ? median->next : median;

	// Construct edge table
	EdgeTable * et = new EdgeTable(min->vec[1], NULL, new EdgeTable(median->vec[1], NULL, NULL));
	double min_max_inc = min->vec[0] == max->vec[0] ? 0.0 : 1.0/slope(min->vec, max->vec);
	ActiveEdgeList * min_max = new ActiveEdgeList(max->vec[1], min->vec[0], min_max_inc, NULL);
	double min_median_inc = min->vec[0] == median->vec[0] ? 0.0 : 1.0/slope(min->vec, median->vec);
	ActiveEdgeList * min_median = new ActiveEdgeList(median->vec[1], min->vec[0], min_median_inc, NULL);
	if (min_max->x < min_median->x ||
		(min_max->x == min_median->x &&
		min_max->x_inc < min_median->x_inc)) {
		et->ael = min_max;
		min_max->next = min_median;
	}
	else {
		et->ael = min_median;
		min_median->next = min_max;
	}
	double median_max_inc = median->vec[0] == max->vec[0] ? 0.0 : 1.0/slope(median->vec, max->vec);
	ActiveEdgeList * median_max = new ActiveEdgeList(max->vec[1], median->vec[0], median_max_inc, NULL);
	et->next->ael = median_max;

	return et;
}

// Returns z-value of face at (x,y)
double get_z(Face * face, int x, int y) {
	Vector n = get_normal(face);
	Vector p = face->pts->vec;
	double k = n[0] * p[0] + n[1] * p[1] + n[2] * p[2];
	double z = (k - n[0] * (double)x - n[1] * (double)y) / n[2];
	delete[] n;
	return z;
}

// Renders face using edge coherence
void draw_face(Face * face) {

	// Sort vertices
	Vector min_x, max_x, min_y, max_y; min_x = max_x = min_y = max_y = face->pts->vec;
	for (Vertex * v = face->pts; v; v = v->next) {
		if (v->vec[0] < min_x[0] ||
			(v->vec[0] == min_x[0] &&
			v->vec[1] < min_x[1]))
			min_x = v->vec;
		if (v->vec[1] < min_y[1] ||
			(v->vec[1] == min_y[1] &&
			v->vec[0] < min_y[0]))
			min_y = v->vec;
		if (v->vec[1] > max_y[1] ||
			(v->vec[1] == max_y[1] &&
			v->vec[0] > max_y[0]))
			max_y = v->vec;
		if (v->vec[0] > max_x[0] ||
			(v->vec[0] == max_x[0] &&
			v->vec[1] > max_x[1]))
			max_x = v->vec;
	}
	Vertex * min, * max; min = max = face->pts;
	for (Vertex * v = face->pts; v; v = v->next) {
		min = v->vec[1] < min->vec[1] ? v : min;
		max = v->vec[1] > max->vec[1] ? v : max;
	}

	// Render face
	EdgeTable * et = get_edge_table(face);
	for (int y = min_y[1]; y < max_y[1] && y < HEIGHT; ++y) {
		// Fill pixels using z-buffer algorithm
		if (y >= 0) {
			for (int x = et->ael->x < 0 ? 0 : et->ael->x; x <= et->ael->next->x && x < WIDTH; ++x) {
				if (x < min_x[0] || x > max_x[0]) continue;
				double z = get_z(face, x, y);
				if (z < frame_buffer[x][y] && z >= 0) {
					frame_buffer[x][y] = z;
					write_pixel(x, y, face->color);
				}
			}
		}

		// Delete finished edges
		if (y == et->ael->y_upper) {
			if (y == et->ael->next->y_upper) break;
			et->next->ael->next = et->ael->next;
			ActiveEdgeList * ael_del = et->ael;
			et->ael = et->next->ael;
			delete ael_del;

			EdgeTable * et_del = et->next;
			et->next = NULL;
			delete et_del;
		}
		else if (y == et->ael->next->y_upper) {
			ActiveEdgeList * ael_del = et->ael->next;
			et->ael->next = et->next->ael;
			delete ael_del;

			EdgeTable * et_del = et->next;
			et->next = NULL;
			delete et_del;
		}

		// Update x
		et->ael->x += et->ael->x_inc;
		et->ael->next->x += et->ael->next->x_inc;
	}

	delete et->ael->next;
	delete et->ael;
	delete et;
}

// Renders line using double scan conversion
void draw_line(Vertex * v1, Vertex * v2, Pixel * color) {
	// Sort vertices
	Vector min_x, max_x, min_y, max_y;
	if (v1->vec[0] < v2->vec[0] ||
		(v1->vec[0] == v2->vec[0] &&
		v1->vec[1] < v2->vec[1])) {
		min_x = v1->vec;
		max_x = v2->vec;
	}
	else {
		min_x = v2->vec;
		max_x = v1->vec;
	}
	if (v1->vec[1] < v2->vec[1] ||
		(v1->vec[1] == v2->vec[1] &&
		v1->vec[0] < v2->vec[0])) {
		min_y = v1->vec;
		max_y = v2->vec;
	}
	else {
		min_y = v2->vec;
		max_y = v1->vec;
	}

	// Get values for scan conversion
	double m = slope(v1->vec, v2->vec);
	double x = min_y[0], y = min_x[1];

	// x-axis scan conversion
	for (int i = min_x[0]; i <= max_x[0] && (m < 0 ? y >= min_y[1] : y <= max_y[1]); ++i) {
		if (min_x[1] < max_x[1])
			for (int j = y; j <= y+m && j <= max_x[1]; ++j)
				write_pixel(i, j, color);
		else
			for (int j = y; j >= y+m && j >= max_x[1]; --j)
				write_pixel(i, j, color);
		y += m;
	}

	// y-axis scan conversion
	for (int j = min_y[1]; j < max_y[1] && (m < 0 ? x >= min_x[0] : x <= max_x[0]); ++j) {
		if (min_y[0] < max_y[0])
			for (int i = x; i <= x+1.0/m && i <= max_y[0]; ++i)
				write_pixel(i, j, color);
		else
			for (int i = x; i >= x+1.0/m && i >= max_y[0]; --i)
				write_pixel(i, j, color);
		x += 1.0/m;
	}
}

// Renders wireframe for face
void draw_wire(Face * face) {
	draw_line(face->pts, face->pts->next, face->color);
	draw_line(face->pts, face->pts->next->next, face->color);
	draw_line(face->pts->next, face->pts->next->next, face->color);
}

// Renders frame
void display() {
	// Clear Screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset frame buffer
	for (int i = 0; i < HEIGHT; ++i)
		for (int j = 0; j < WIDTH; ++j)
			frame_buffer[i][j] = furthest * 100.0;

	// Draw faces
	for (Face * face = faces; face; face = face->next)
		wire ? draw_wire(face) : draw_face(face);

	// Draw Frame Buffer
	glutSwapBuffers();
}


/************
  --- UI ---
 ************/
// Translates vertices according to command
void translate(Command command) {
	switch (command) {
		case UP: translate(0.0, 5.0, 0.0); break;
		case DOWN: translate(0.0, -5.0, 0.0); break;
		case LEFT: translate(-5.0, 0.0, 0.0); break;
		case RIGHT: translate(5.0, 0.0, 0.0); break;
		default:;
	}
}

// Rotates vertices according to command
void rotate(Command command) {
	switch (command) {
		case UP:
			translate_to_origin();
			rotate_x(M_PI / 32.0);
			translate_back();
			break;
		case DOWN:
			translate_to_origin();
			rotate_x(-M_PI / 32.0);
			translate_back();
			break;
		case LEFT:
			translate_to_origin();
			rotate_y(M_PI / 32.0);
			translate_back();
			break;
		case RIGHT:
			translate_to_origin();
			rotate_y(-M_PI / 32.0);
			translate_back();
			break;
		default:;
	}
}

// Scales vertices according to command
void scale(Command command) {
	switch (command) {
		case UP: case RIGHT:
			translate_to_origin();
			scale(1.05);
			translate_back();
			break;
		case DOWN: case LEFT:
			translate_to_origin();
			scale(0.95);
			translate_back();
			break;
		default:;
	}
}

// Parses command and acts accordingly
void command(Command command) {
	switch (command) {
		case NONE: break;
		case WIREFRAME:
			wire = !wire;
			cout << (wire ? "Wireframe " : "Colored ") << "mode\n";
			break;
		case TRANSLATE:
			mode = mode == TRANSLATE ? NONE : TRANSLATE;
			cout << (mode ? "Translation " : "Viewing ") << "mode\n";
			break;
		case ROTATE:
			mode = mode == ROTATE ? NONE : ROTATE;
			cout << (mode ? "Rotation " : "Viewing ") << "mode\n";
			break;
		case SCALE:
			mode = mode == SCALE ? NONE : SCALE;
			cout << (mode ? "Scaling " : "Viewing ") << "mode\n";
			break;
		case UP: case DOWN: case LEFT: case RIGHT:
			switch (mode) {
				case TRANSLATE: translate(command); break;
				case ROTATE: rotate(command); break;
				case SCALE: scale(command); break;
				default:;
			} break;
	}
}

// Keyboard driver
void keyboard (unsigned char key, int x, int y) {
	switch (key) {
		case 27: // ESC
			exit (0);
	    case '1': // Stub for new screen
		    printf("New screen\n");
			break;
		case 'a': command(mode ? LEFT : NONE); break;
		case 's': command(mode ? DOWN : NONE); break;
		case 'd': command(mode ? RIGHT : NONE); break;
		case 'w': command(mode ? UP : WIREFRAME); break;
		case 't': command(TRANSLATE); break;
		case 'r': command(ROTATE); break;
		case 'e': command(SCALE); break;
		case 'q':
			mode = NONE;
			cout << "Viewing mode\n";
	}
}


/*****************
  --- Parsing ---
 *****************/
// Parses vertices from file
void getVertices(char * file_name) {
	// Prepare for parsing
	string type;
	ifstream file; file.open(file_name);

	// Parse for vertices
	while (file >> type)
		if (type == "v") {
			double x, y, z;
			file >> x >> y >> z;
			Vector v = new double[4];
			v[0] = x; v[1] = y; v[2] = z; v[3] = 1.0;
			vertices = new Vertex(v, vertices);
		}

	// Calculate furthest distance between vertices
	for (Vertex * i = vertices; i; i = i->next)
		for (Vertex * j = i; j; j = j->next) {
			double d = distance(i->vec, j->vec);
			furthest = d > furthest ? d : furthest;
		}

	// Close file
	file.close();
}

// Normalizes shape by scaling as necessary and centering in window
void normalize() {

	// Find values to use in transformations
	for (Vertex * curr = vertices; curr->next; curr = curr->next) {
		double d = distance(curr->vec, curr->next->vec);
		furthest = d > furthest ? d : furthest;
	}
	int min_dimension = WIDTH < HEIGHT ? WIDTH : HEIGHT;
	double scale_factor = 4 * min_dimension / 5 / furthest;

	// Translate to origin
	translate_to_origin();
	// Scale to reasonable size
	scale(scale_factor);
	// Translate to middle
	translate_to_center();
}

// Parses faces from file
void getFaces(char * file_name) {
	// Prepare for parsing
	string type;
	ifstream file; file.open(file_name);

	// Parse for faces
	while (file >> type)
		if (type == "f") {
			faces = new Face(NULL, faces);
			for (int j = 0; j < 3; ++j) {
				int i; char c;
				file >> i;
				Vertex * pt = NULL;
				for (Vertex * curr = vertices; curr && !pt; curr = curr->next)
					pt = curr->length == i ? curr : NULL;
				faces->pts = new Vertex(pt->vec, faces->pts);
				file >> noskipws;
				while (file >> c && c != ' ' && c != '\n');
				file >> skipws;
			}
		}

	// Close file
	file.close();
}


/**************
  --- Main ---
 **************/
int main (int argc, char *argv[]) {
	// OpenGL initializations
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Computer Graphics");
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutKeyboardFunc(keyboard);

	// Read in from file
	if (argc != 2) {
		cerr << "Usage: ./assn2 <filename>. Exiting." << endl;
		exit(1);
	}
	getVertices(argv[1]);
	normalize();
	getFaces(argv[1]);

	// Set up data structures
	frame_buffer = new Vector[WIDTH];
	for (int i = 0; i < WIDTH; ++i) {
		frame_buffer[i] = new double[HEIGHT];
		for (int j = 0; j < HEIGHT; ++j)
			frame_buffer[i][j] = furthest + 50.0;
	}

	// Create window
	glClearColor(0.0,0.0,0.0,0.0);
	glShadeModel(GL_SMOOTH);
	glOrtho(0,WIDTH,0,HEIGHT,-1.0,1.0);

	// Initialize The Main Loop
	glutMainLoop();
}

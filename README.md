# .obj File Renderer

## Modes
There are four transformation modes you can choose between:

	1. Viewing mode (default)
		- Toggle between viewing modes by pressing 'w':
			1. Wireframe
				- While in wireframe mode, only the outline of the shape
				  appears.
			2. Colored (z-buffer)
				- While in colored mode, the entire shape appears as projected
				  onto the z-buffer.
		- Turn on viewing mode by pressing 'q' or toggling out of another mode.
	2. Translation mode
		- Translate the shape in the x-direction by pressing 'a' or 'd'.
		- Translate the shape in the y-direction by pressing 's' or 'w'.
		- Toggle translation mode by pressing 't'.
	3. Rotation mode
		- Rotate the shape about the y-axis by pressing 'a' or 'd'.
		- Rotate the shape about the x-axis by pressing 's' or 'w'.
		- Toggle rotation mode by pressing 'r'.
	4. Scaling mode
		- Scale the shape positively by pressing 'w' or 'd'.
		- Scale the shape negatively by pressing 's' or 'a'.
		- Toggle scaling mode by pressing 'e'.

You may only be in one mode at a time

## File Summary
assn2.cpp - All source code is contained in this file.

Makefile - Compiling script.

## Commandline
Compile: `make`

Execute: `./assn2 <filename>`
 - replace `<filename>` with the name of the wavefront obj file you wish to
   upload.

## Known Limitations
Parsing
- The parser will only gather triangular faces. The program will not work
  otherwise.
- The parser assumes the file given is valid and contains no errors.
  Rendering
- The more of the screen the shape covers, the worse the performance.
